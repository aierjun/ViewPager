package com.aierjun.viewpager;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by aierJun on 2016/12/4.
 */
public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
